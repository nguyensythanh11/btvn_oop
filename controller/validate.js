let showMessage = (spanId,message) => {
    document.getElementById(spanId).style.display = "block";
    document.getElementById(spanId).innerHTML = message;
}

let checkLoopId = (listPerson, newPerson) => {
    if(newPerson.id.length == 0){
        showMessage("idVal","Please enter your id");
        return false;
    }
    let index = listPerson.findIndex((person) => { 
        return newPerson.id == person.id;
    })
    if(index == -1){
        showMessage("idVal","");
        return true;
    }
    showMessage("idVal","Id had valid !!!");
    return false;
}

let checkName = (person) => {
    let length = person.name;
    if(length == 0){
        showMessage("nameVal","Please enter your name");
        return false;
    }
    const re = /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễếệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u;
    if(re.test(person.name)){
        showMessage("nameVal","");
        return true;
    }
    showMessage("nameVal","Name consists only of letters");
    return false;
}

let checkAddress = (person) => {
    if(person.address == 0){
        showMessage("addressVal","Please enter your address");
        return false;
    }
    showMessage("addressVal","");
    return true;
}

let checkEmail = (person) => {
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(person.email.length == 0){
        showMessage("emailVal","Please enter your email")
        return false;
    }
    if(re.test(person.email)){
        showMessage("emailVal","");
        return true;
    }
    showMessage("emailVal", "Please enter correct fomat of email");
    return false;
}

let checkScore = (score,spanId) => {
    if(score < 0 || score > 10){
        showMessage(spanId,"Score from 0 to 10");
        return false;
    }
    const re = /^\d+$/
    if(re.test(score)){
        showMessage(spanId,"");
        return true;
    }
    showMessage(spanId,"Please enter correct fomat of score");
    return false;
}

let checkDaySalary = (daySalary,spanId,message) => {
    const re = /^\d+$/
    if(re.test(daySalary)){
        showMessage(spanId,"");
        return true;
    }
    showMessage(spanId,`Please enter correct fomat of ${message}`);
    return false;
}

let checkNameCompany = (person) => {
    if(person.nameCompany.length == 0){
        showMessage("nameCompanyVal","Please enter your name of company");
        return false;
    }
    showMessage("nameCompanyVal","");
    return true;
}

let checkEvaluate = (person) => {
    if(person.evaluate.length == 0){
        showMessage("evaluateVal","Please enter your evaluate");
        return false;
    }
    showMessage("evaluateVal","");
    return true;
}

let checkPrice = (person) => {
    if(person.price == "0"){
        showMessage("priceBillVal",`Please enter the price`);
        return false;
    }
    const re = /^\d+$/
    if(re.test(person.price)){
        showMessage("priceBillVal","");
        return true;
    }
    showMessage("priceBillVal",`Price includes only digits`);
    return false;
}

let checkAll = (listPerson,person) => {
    let isValid = checkLoopId(listPerson,person);
    isValid = isValid & checkName(person);
    isValid = isValid & checkAddress(person);
    isValid = isValid & checkEmail(person);
    if(person.job == "student"){
        isValid = isValid & checkScore(person.math, "mathVal") & checkScore(person.physic, "physicVal") & checkScore(person.chemistry, "chemistryVal");
    }
    else if(person.job == "employee"){
        isValid = isValid & checkDaySalary(person.dayWorking,"dayWorkingVal","days of working") & checkDaySalary(person.salaryDay,"salaryDayVal","salaryDay of one day");
    }
    else if(person.job == "customer"){
        isValid = isValid & checkNameCompany(person) & checkPrice(person) & checkEvaluate(person);
    }
    return isValid;
}

export default checkAll;