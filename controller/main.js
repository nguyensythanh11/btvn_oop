import { Student } from "../model/model.js";
import { getDetailFromForm, getLocalStorage, renderPersonList, saveLocalStorage, showDetailFromForm } from "./controller.js";
import checkAll from "./validate.js";

let listPerson = [];

listPerson = getLocalStorage();
renderPersonList(listPerson);

let openModal = () => {
    document.getElementById("student").style.display = "none";
    document.getElementById("employee").style.display = "none";
    document.getElementById("customer").style.display = "none";
}
window.openModal = openModal;

let fomatPerson = (person) => {
    if(person.job == "student"){
        let {
            name,
            address,
            id,
            email,
            job, 
            math,
            physic,
            chemistry
        } = person
        return new Student(name,address,id,email,job,math,physic,chemistry);
    }
    else if(person.job == "employee"){
        return {... person, caculateSalary(){
            return person.salaryDay*person.dayWorking;
        }}
    }
    else return {... person};
}
window.fomatPerson = fomatPerson;

let changeJob = () => {
    let job = document.getElementById("job").value;
    if(job == "student"){
        document.getElementById("student").style.display = "flex";
        document.getElementById("employee").style.display = "none";
        document.getElementById("customer").style.display = "none";
    }
    else if(job == "employee"){
        document.getElementById("student").style.display = "none";
        document.getElementById("employee").style.display = "flex";
        document.getElementById("customer").style.display = "none";
    }
    else if(job == "customer"){
        document.getElementById("student").style.display = "none";
        document.getElementById("employee").style.display = "none";
        document.getElementById("customer").style.display = "flex";
    }
    else if(job == "other"){
        document.getElementById("student").style.display = "none";
        document.getElementById("employee").style.display = "none";
        document.getElementById("customer").style.display = "none";
    }
}
window.changeJob = changeJob;

let createPerson = () => {
    let person = getDetailFromForm();
    // console.log("🚀 ~ file: main.js:67 ~ createPerson ~ person:", person.name)
    
    let isValid = checkAll(listPerson,person);
    if(isValid){
        listPerson.push(person);
        renderPersonList(listPerson);
        saveLocalStorage(listPerson);
        $('#myModal').modal('hide');
        resetForm();
    }
}
window.createPerson = createPerson;

let closeButton = () => { 
    $('#myModal').modal('hide');
    resetForm();
}
window.closeButton = closeButton;

let resetForm = () => {
    document.querySelector("#myForm").reset();
}
window.resetForm = resetForm;

let deletePerson = (id) => {
    let index = listPerson.findIndex((person) => {
        return id == person.id;
    })
    listPerson.splice(index,1);
    renderPersonList(listPerson);
    saveLocalStorage(listPerson);
}
window.deletePerson = deletePerson;

let modifyPerson = (id) => {
    let index = listPerson.findIndex((person) => {
        return id == person.id;
    })
    openModal();
    showDetailFromForm(listPerson[index]);
    $('#myModal').modal('show');
    document.getElementById("id").disabled = true;
}
window.modifyPerson = modifyPerson;

let updatePerson = () => {
    let personUpdate = getDetailFromForm();
    let indexUpdate = listPerson.findIndex((person) => {
        return personUpdate.id == person.id;
    })
    if(personUpdate.job == "student"){
        let {
            name,
            address,
            id,
            email,
            job, 
            math,
            physic,
            chemistry
        } = personUpdate
        listPerson[indexUpdate] = new Student(name,address,id,email,job,math,physic,chemistry);
    }
    else if(personUpdate.job == "employee"){
        listPerson[indexUpdate] = {... personUpdate, caculateSalary(){
            return personUpdate.salaryDay*personUpdate.dayWorking;
        }}
    }
    else listPerson[indexUpdate] = {... person};
    renderPersonList(listPerson);
    saveLocalStorage(listPerson);
    closeButton();
}
window.updatePerson = updatePerson;

let sortName = () => {
    let newListPerson = [... listPerson];
  
    for(let i=0; i<newListPerson.length-1; i++){
      for(let j = i+1; j<newListPerson.length; j++){
        let name_1 = newListPerson[i].name;
        let name_2 = newListPerson[j].name;
        if(name_1 > name_2){
          let temp = newListPerson[i];
          newListPerson[i] = newListPerson[j];
          newListPerson[j] = temp;
        }
      }
    }
    renderPersonList(newListPerson);
}
window.sortName = sortName;

let changeFilter = () => {
    let job = document.getElementById("filter").value;
    let newListPerson = [];
    listPerson.forEach((person) => { 
        job == person.job && newListPerson.push(fomatPerson(person));
    })
    renderPersonList(newListPerson);
}
window.changeFilter = changeFilter;