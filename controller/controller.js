import { Customer, Employee, Person, Student } from "../model/model.js";

export let getDetailFromForm = () => {
    let name = document.getElementById("name").value;
    let address = document.getElementById("address").value;
    let id = document.getElementById("id").value;
    let email = document.getElementById("email").value;
    let job = document.getElementById("job").value;
    if(job == "student"){
        let math = document.getElementById("math").value*1;
        let physic = document.getElementById("physic").value*1;
        let chemistry = document.getElementById("chemistry").value*1;
        return new Student(name,address,id,email,job,math,physic,chemistry);
    }
    else if(job == "employee"){
        let dayWorking = document.getElementById("dayWorking").value*1;
        let salaryDay = document.getElementById("salaryDay").value*1;
        return new Employee(name,address,id,email,job,dayWorking,salaryDay);
    }
    else if(job == "customer"){
        let nameCompany = document.getElementById("nameCompany").value;
        let price = document.getElementById("priceBill").value*1;
        let evaluate = document.getElementById("evaluate").value;
        return new Customer(name,address,id,email,job,nameCompany,price,evaluate);
    }
    else if(job == "other"){
        return new Person(name,address,id,email,job);
    }
}

export let renderPersonList = (listPerson) => {
    let contentHTML = "";
    listPerson.forEach((person) => {
        let {
            name,
            address,
            id,
            email,
            job
        } = person;
        let content = 
        `
            <tr>
                <td>${id}</td>
                <td>${name}</td>
                <td>${address}</td>
                <td>${email}</td>
                <td>${job}</td>
                <td>${job == "student" ? person.caculateDTB() : "Not Given"}</td>
                <td>${job == "employee" ? person.caculateSalary() : "Not Given"}</td>
                <td class = "w-52">
                    <button onclick = "deletePerson('${id}')" class = "btn btn-danger">Delete</button>
                    <button onclick = "modifyPerson('${id}')" class = "btn btn-warning">Modify</button>
                </td>
            </tr>        
        `
        contentHTML += content;
    });
    document.getElementById("DanhsachPerson").innerHTML = contentHTML;
}

export let saveLocalStorage = (listPerson) => { 
    let dataJson = JSON.stringify(listPerson);
    localStorage.setItem("DSND",dataJson);
}

export let getLocalStorage = () => {
    let dataJson = localStorage.getItem("DSND");
    if(dataJson != null){
        let listPerson = JSON.parse(dataJson);
        listPerson = listPerson.map((person) => {
            if(person.job == "student"){
                let {
                    name,
                    address,
                    id,
                    email,
                    job, 
                    math,
                    physic,
                    chemistry
                } = person
                return new Student(name,address,id,email,job,math,physic,chemistry);
            }
            else if(person.job == "employee"){
                return {... person, caculateSalary(){
                    return person.salaryDay*person.dayWorking;
                }}
            }
            else return {... person};
        })
        return listPerson;
    }
    return [];
}

export let showDetailFromForm = (person) => {
    if(person.job == "student"){
        let {
            name,
            address,
            id,
            email,
            job, 
            math,
            physic,
            chemistry
        } = person;
        document.getElementById("student").style.display = "flex";
        document.getElementById("name").value = name;
        document.getElementById("address").value = address;
        document.getElementById("id").value = id;
        document.getElementById("email").value = email;
        document.getElementById("job").value = job;
        document.getElementById("math").value = math;
        document.getElementById("physic").value = physic;
        document.getElementById("chemistry").value = chemistry;
    }
    else if(person.job == "employee"){
        let {
            name,
            address,
            id,
            email,
            job, 
            dayWorking,
            salaryDay
        } = person;
        document.getElementById("employee").style.display = "flex";
        document.getElementById("name").value = name;
        document.getElementById("address").value = address;
        document.getElementById("id").value = id;
        document.getElementById("email").value = email;
        document.getElementById("job").value = job;
        document.getElementById("dayWorking").value = dayWorking;
        document.getElementById("salaryDay").value = salaryDay;
    }
    else if(person.job == "customer"){
        let {
            name,
            address,
            id,
            email,
            job, 
            nameCompany,
            price,
            evaluate
        } = person;
        document.getElementById("customer").style.display = "flex";
        document.getElementById("name").value = name;
        document.getElementById("address").value = address;
        document.getElementById("id").value = id;
        document.getElementById("email").value = email;
        document.getElementById("job").value = job;
        document.getElementById("nameCompany").value = nameCompany;
        document.getElementById("priceBill").value = price;
        document.getElementById("evaluate").value = evaluate;
    }
    else{
        let {
            name,
            address,
            id,
            email,
            job, 
        } = person;
        document.getElementById("student").style.display = "flex";
        document.getElementById("name").value = name;
        document.getElementById("address").value = address;
        document.getElementById("id").value = id;
        document.getElementById("email").value = email;
        document.getElementById("job").value = job;
    }
}