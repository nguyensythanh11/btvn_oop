export class Person{
    constructor(name,address,id,email,job){
        this.name = name;
        this.address = address;
        this.id = id;
        this.email = email;
        this.job = job
    }
}

export class Student extends Person{
    constructor(name,address,id,email,job,math,physic,chemistry){
        super(name,address,id,email,job);
        this.math = math;
        this.physic = physic;
        this.chemistry = chemistry;
    }
    caculateDTB(){
        return (this.math + this.physic + this.chemistry)/3
    }
}

export class Employee extends Person{
    constructor(name,address,id,email,job,dayWorking,salaryDay){
        super(name,address,id,email,job);
        this.dayWorking = dayWorking;
        this.salaryDay = salaryDay;
    }
    caculateSalary(){
        return this.salaryDay*this.dayWorking;
    }
}

export class Customer extends Person{
    constructor(name,address,id,email,job,nameCompany,price,evaluate){
        super(name,address,id,email,job);
        this.nameCompany = nameCompany;
        this.price = price;
        this.evaluate = evaluate;
    }
}